#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
FILE_NAME=$(basename "$0")

TMP=$(mktemp -d 2>/dev/null || mktemp -d -t $FILE_NAME)
LOCK_PATH="/tmp/$FILE_NAME.pid"

shopt -s nullglob

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
EXPAND_TO=60

function lock() {
  if [[ -f "$LOCK_PATH" ]]; then
    echo >&2 "There's another instance of the script in progress. If this is incorrect please remove lock file '$LOCK_PATH' and try again"
    exit 1;
  fi

  echo $$ > $LOCK_PATH
  trap "release_lock" INT TERM EXIT
}

function release_lock() {
  if [[ -f "$LOCK_PATH" ]]; then
    rm $LOCK_PATH
  fi
}

function print_execution_mode() {
  if [[ -z "${PARALLEL}" ]]; then
    echo "$FILE_NAME: Using sequential mode [default]"
  else
    echo "$FILE_NAME: Using parallel mode"
  fi
  echo
}

function cmd() {
  if [[ -z "${PARALLEL}" ]]; then
    seq_cmd "$@"
  else
    par_cmd "$@"
  fi
}

function wait_for() {
  pod_name="$1"
  url="$2"

  for i in {1..50}
  do
    code=$(kubectl exec $pod_name -- curl -i -k --max-time 1 --silent --output /dev/stderr --write-out "%{http_code}" "$url" 2> /dev/null)
    st=$?

    if [[ "$st" -eq "0" && "$code" -lt "400" ]]; then
      return 0
    fi

    sleep 0.25
  done

  return 1
}

# This function blocks the execution to make sure that background `cmd` calls are completed
# This is a no-op for sequential mode
wait_cmd() {
  if [[ -z "${PARALLEL}" ]]; then
    # Do nothing if parallel mode is enabled
    return
  fi

  local error_detected=false

  for pid_file in $TMP/$$.*.pid; do
    pid=$(cat $pid_file)
    wait $pid

    base_file=$(echo $pid_file | sed 's/.pid$//')
    name=$(cat $base_file.name)
    st=`cat $base_file.st`
    if [ $st -eq 0 ]; then
      printf "> $name\t[${GREEN}OK${NC}]\n" | expand -t $EXPAND_TO
    else 
      printf "> $name\t[${RED}FAIL${NC}]\n" $msg | expand -t $EXPAND_TO
      output=`cat $base_file.output`
      command=$(cat $base_file.cmd)
      echo -e "Exit code: $st. '$name' failed: \`$command\` with:\n$output"
      error_detected=true
    fi

    rm $base_file.*
  done

  if [ "$error_detected" = true ] ; then
    echo "Failures detected. Aborting the execution"
    exit 1
  fi
}

function seq_cmd() {
  printf "> $1 ...\t" | expand -t 60
  shift

  output=`{ eval "$@" 2>&1; echo $? > $TMP/ps.$$; }`
  st=`cat $TMP/ps.$$;rm $TMP/ps.$$`

  if [ $st -eq 0 ]; then
    printf "[${GREEN}OK${NC}]\n"
  else 
    printf "[${RED}FAIL${NC}]\n"
    echo $output
    echo "Exit code: $st. Failed command: \`$@\`"
    echo "Aborting the execution"
    exit 1
  fi
}

function par_cmd() {
  args=$@
  name=$1
  shift
  command=$@
  sha=$(echo -n "$command" | md5sum | awk '{print $1}')

  { output=`{ eval "$@" 2>&1; echo $? > $TMP/$$.$sha.st; }`; echo -n "$output" > $TMP/$$.$sha.output; } &
  PID=$!

  echo -n "$PID" > $TMP/$$.$sha.pid
  echo -n "$name" > $TMP/$$.$sha.name
  echo -n "$command" > $TMP/$$.$sha.cmd
}
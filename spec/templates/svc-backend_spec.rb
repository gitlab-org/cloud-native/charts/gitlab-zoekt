require 'spec_helper'
require 'helm_template_helper'

describe 'templates/svc-backend.yaml' do
  let(:path) { 'Service/test-gitlab-zoekt' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  it 'creates the resource and adds labels', :aggregate_failures do
    t = HelmTemplate.new(default_values)

    expect(t.resource_exists?(path)).to eq(true)
    expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
    expect(t.labels(path)).to have_key('helm.sh/chart')
    expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
    expect(t.labels(path)).to have_key('app.kubernetes.io/version')
  end

  describe 'when service.annotations is set' do
    let(:values) do
      YAML.safe_load(%(
        service:
          annotations:
            external-dns.alpha.kubernetes.io/hostname: "zoekt.example.com."
      )).merge(default_values)
    end

    it 'does not add annotations to the backend svc' do
      t = HelmTemplate.new(values)

      expect(t.resource_exists?(path)).to eq(true)
      expect(t.annotations(path)).to be_nil
    end
  end

  describe 'when serviceBackend.annotations is set' do
    let(:values) do
      YAML.safe_load(%(
        serviceBackend:
          annotations:
            external-dns.alpha.kubernetes.io/hostname: "zoekt-backend.example.com."
      )).merge(default_values)
    end

    it 'adds annotations to the backend svc' do
      t = HelmTemplate.new(values)

      expect(t.resource_exists?(path)).to eq(true)
      expect(t.annotations(path)).to include('external-dns.alpha.kubernetes.io/hostname' => 'zoekt-backend.example.com.')
    end
  end
end
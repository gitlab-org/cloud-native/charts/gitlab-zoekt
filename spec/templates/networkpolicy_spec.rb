require 'spec_helper'
require 'helm_template_helper'

describe 'templates/networkpolicy.yaml' do
  let(:path) { 'NetworkPolicy/test-gitlab-zoekt' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  describe 'networkpolicy.enabled' do
    let(:values) do
      YAML.safe_load(%(
        networkpolicy:
          enabled: #{network_policy}
      )).merge(default_values)
    end

    context 'when true' do
      let(:network_policy) { true }

      it 'creates a networkpolicy' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.dig(path)).not_to be nil
      end

      it 'adds labels', :aggregate_failures do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.resource_exists?(path)).to eq(true)
        expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
        expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
        expect(t.labels(path)).to have_key('helm.sh/chart')
        expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
        expect(t.labels(path)).to have_key('app.kubernetes.io/version')
      end
    end

    context 'when false' do
      let(:network_policy) { false }

      it 'does not create a networkpolicy' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.dig(path)).to be nil
      end
    end
  end

  describe 'networkpolicy.egress.enabled' do
    let(:values) do
      YAML.safe_load(%(
        networkpolicy:
          enabled: true
          egress:
            enabled: #{egress}
      )).merge(default_values)
    end

    context 'when true' do
      let(:egress) { true }

      it 'adds Egress to policyTypes' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.dig(path, 'spec', 'policyTypes')).to include('Egress')
      end
    end

    context 'when false' do
      let(:egress) { false }

      it 'does not add Egress to policyTypes' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.dig(path, 'spec', 'policyTypes')).to be_nil
      end
    end
  end

  describe 'networkpolicy.ingress.enabled' do
    let(:values) do
      YAML.safe_load(%(
        networkpolicy:
          enabled: true
          ingress:
            enabled: #{ingress}
      )).merge(default_values)
    end

    context 'when true' do
      let(:ingress) { true }

      it 'adds Ingress to policyTypes' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.dig(path, 'spec', 'policyTypes')).to include('Ingress')
      end
    end

    context 'when false' do
      let(:ingress) { false }

      it 'does not add Ingress to policyTypes' do
        t = HelmTemplate.new(values)
        expect(t.exit_code).to eq(0)

        expect(t.dig(path, 'spec', 'policyTypes')).to be_nil
      end
    end
  end
end
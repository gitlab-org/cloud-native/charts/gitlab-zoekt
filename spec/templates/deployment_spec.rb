require 'spec_helper'
require 'helm_template_helper'

describe 'templates/deployment.yaml' do
  let(:path) { 'Deployment/test-gitlab-zoekt-gateway' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  it 'includes the defaults' do
    t = HelmTemplate.new(default_values)

    expect(t.exit_code).to eq(0)
    expect(t.resource_exists?(path)).to eq(true)
    expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
    expect(t.labels(path)).to have_key('helm.sh/chart')
    expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
    expect(t.labels(path)).to have_key('app.kubernetes.io/version')
    expect(t.annotations(path)).to be_nil
  end

  it_behaves_like 'builds gateway image', 'zoekt-external-gateway'

  context 'when deploymentAnnotations has multiple values' do
    let(:values) do
      YAML.safe_load(%(
        deploymentAnnotations:
          secret.reloader.stakater.com/reload: some-cert
          foo: bar
      )).merge(default_values)
    end

    it 'includes the annotations' do
      t = HelmTemplate.new(values)

      expect(t.exit_code).to eq(0)
      expect(t.resource_exists?(path)).to eq(true)
      expect(t.annotations(path)).to include('secret.reloader.stakater.com/reload' => 'some-cert', 'foo' => 'bar')
    end
  end

  context 'when nodeSelector is configured' do
    let(:values) do
      YAML.safe_load(%(
        nodeSelector:
          disktype: ssd
          kubernetes.io/arch: amd64
      )).merge(default_values)
    end

    it 'sets the nodeSelector in the deployment' do
      t = HelmTemplate.new(values)

      expect(t.exit_code).to eq(0)
      expect(t.resource_exists?(path)).to eq(true)
      node_selector = t.dig(path, 'spec', 'template', 'spec', 'nodeSelector')
      expect(node_selector).to include('disktype' => 'ssd', 'kubernetes.io/arch' => 'amd64')
    end
  end

  context 'when affinity is configured' do
    let(:values) do
      YAML.safe_load(%(
        affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                - key: kubernetes.io/e2e-az-name
                  operator: In
                  values:
                  - e2e-az1
                  - e2e-az2
      )).merge(default_values)
    end

    it 'sets the affinity in the deployment' do
      t = HelmTemplate.new(values)

      expect(t.exit_code).to eq(0)
      expect(t.resource_exists?(path)).to eq(true)
      affinity = t.dig(path, 'spec', 'template', 'spec', 'affinity')
      expect(affinity).to include('nodeAffinity')
      node_affinity = affinity['nodeAffinity']
      expect(node_affinity).to have_key('requiredDuringSchedulingIgnoredDuringExecution')
    end
  end

  context 'when tolerations are configured' do
    let(:values) do
      YAML.safe_load(%(
        tolerations:
        - key: "key1"
          operator: "Equal"
          value: "value1"
          effect: "NoSchedule"
        - key: "key2"
          operator: "Exists"
          effect: "NoExecute"
          tolerationSeconds: 3600
      )).merge(default_values)
    end

    it 'sets the tolerations in the deployment' do
      t = HelmTemplate.new(values)

      expect(t.exit_code).to eq(0)
      expect(t.resource_exists?(path)).to eq(true)
      tolerations = t.dig(path, 'spec', 'template', 'spec', 'tolerations')
      expect(tolerations).to be_an(Array)
      expect(tolerations.length).to eq(2)
      expect(tolerations[0]).to include('key' => 'key1', 'operator' => 'Equal', 'value' => 'value1', 'effect' => 'NoSchedule')
      expect(tolerations[1]).to include('key' => 'key2', 'operator' => 'Exists', 'effect' => 'NoExecute', 'tolerationSeconds' => 3600)
    end
  end
end

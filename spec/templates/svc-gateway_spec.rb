require 'spec_helper'
require 'helm_template_helper'

describe 'templates/svc-gateway.yaml' do
  let(:path) { 'Service/test-gitlab-zoekt-gateway' }

  let(:default_values) do
    HelmTemplate.defaults
  end

  it 'creates the resource and adds labels', :aggregate_failures do
    t = HelmTemplate.new(default_values)

    expect(t.resource_exists?(path)).to eq(true)
    expect(t.labels(path)).to include('app.kubernetes.io/name' => 'gitlab-zoekt')
    expect(t.labels(path)).to include('app.kubernetes.io/instance' => 'test')
    expect(t.labels(path)).to have_key('helm.sh/chart')
    expect(t.labels(path)).to have_key('app.kubernetes.io/instance')
    expect(t.labels(path)).to have_key('app.kubernetes.io/version')
  end

  describe 'service.type' do
    let(:cluster_ip) { nil }
    let(:load_balancer_ip) { nil }

    let(:values) do
      YAML.safe_load(%(
        service:
          type: #{service_type}
          clusterIP: #{cluster_ip}
          loadBalancerIP: #{load_balancer_ip}
      )).merge(default_values)
    end

    context 'when ClusterIp' do
      let(:service_type) { 'ClusterIP' }

      context 'when clusterIP is not empty' do
        let(:cluster_ip) { '1.2.3.4' }

        it 'populates clusterIP' do
          t = HelmTemplate.new(values)
          expect(t.exit_code).to eq(0)
          expect(t.dig(path, 'spec', 'clusterIP')).to eq cluster_ip
        end
      end

      context 'when clusterIP is empty' do
        it 'does not populate clusterIP' do
          t = HelmTemplate.new(values)
          expect(t.exit_code).to eq(0)
          expect(t.dig(path, 'spec').keys).not_to include 'clusterIP'
        end
      end
    end

    context 'when LoadBalancer' do
      let(:service_type) { 'LoadBalancer' }

      context 'when loadBalancerIP is not empty' do
        let(:load_balancer_ip) { '4.4.2.2' }

        it 'populates clusterIP' do
          t = HelmTemplate.new(values)
          expect(t.exit_code).to eq(0)
          expect(t.dig(path, 'spec', 'loadBalancerIP')).to eq load_balancer_ip
        end
      end

      context 'when loadBalancerIP is empty' do
        it 'does not populate loadBalancerIP' do
          t = HelmTemplate.new(values)
          expect(t.exit_code).to eq(0)
          expect(t.dig(path, 'spec').keys).not_to include 'loadBalancerIP'
        end
      end
    end
  end
end